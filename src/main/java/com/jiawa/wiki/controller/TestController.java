package com.jiawa.wiki.controller;

import com.jiawa.wiki.domain.Test;
import com.jiawa.wiki.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//一般返回字符串 RestController
//@Controller 一般返回页面
// 返回的状态码 405 一般是访问的请求方式代表不支持
@RestController
public class TestController {
    @Value("${test.hello:wade默认值}")
    private String testWadeHello;

    @Autowired
    private TestService testService;

    @RequestMapping("/hello")
    public String hello(String name) {
        return "hello - world" + testWadeHello;
    }

    @PostMapping("/hello/post")
    public String helloPost(String name) {
        return "hello - world-post" + name;
    }

    @GetMapping("/test/list")
    public List<Test> list(String name) {
        return testService.list();
    }
}
