package com.jiawa.wiki.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiawa.wiki.domain.Ebook;
import com.jiawa.wiki.mapper.EbookMapper;
import com.jiawa.wiki.req.EbookQueryReq;
import com.jiawa.wiki.req.EbookSaveReq;
import com.jiawa.wiki.resp.EbookRueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Slf4j
@Service
public class EbookService {
    @Autowired
    private EbookMapper ebookMapper;

    @Autowired
    private SnowFlake snowFlake;

    public PageResp<EbookRueryResp> list(EbookQueryReq req) {
        // 使用pagehelper分页
        PageHelper.startPage(req.getPage(), req.getSize());
        List<Ebook> ebooksList = ebookMapper.selectAll(req.getName());
        PageInfo<Ebook> pageInfo = new PageInfo<>(ebooksList);
        log.info("总条数：{}", pageInfo.getTotal());
        log.info("总页数：{}", pageInfo.getPages());

        // 使用封装的CopyUtil来合并对象
//        List<EbookResp> respList = new ArrayList<>();
//        for (Ebook ebook : ebooksList) {
//            EbookResp ebookResp = new EbookResp();
//            BeanUtils.copyProperties(ebook,ebookResp);
//            ebookResp.setId(123L);
//            respList.add(ebookResp);
//        使用封装的CopyUtil进行单体复制
//            CopyUtil.copy(ebook,class)
//        }

        List<EbookRueryResp> respList = CopyUtil.copyList(ebooksList, EbookRueryResp.class);
        PageResp<EbookRueryResp> pageContent = new PageResp();
        pageContent.setTotal(pageInfo.getTotal());
        pageContent.setList(respList);
        return pageContent;
    }

    // 编辑报存 or 新增保存
    public void save(EbookSaveReq req) {
        Ebook ebook = CopyUtil.copy(req, Ebook.class);
        //先判断是否有id
        if (ObjectUtils.isEmpty(req.getId())) {
            ebook.setId(snowFlake.nextId());
            ebookMapper.insert(ebook);
        } else {
            ebookMapper.updateByPrimaryKey(ebook);
        }
    }
    //删除
    public void delete(Long id){
        ebookMapper.deleteByPrimaryKey(id);
    }
}
